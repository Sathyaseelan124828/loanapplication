package com.example.experiencems.response;


import com.example.experiencems.Constants.SalaryEnum;
import com.example.experiencems.Constants.StatusEnum;
import com.example.experiencems.Constants.Tenure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRegResponse {


    private StatusEnum statusEnum;


}
