package com.example.experiencems.service;

import com.example.experiencems.Constants.CountryEnum;
import com.example.experiencems.Constants.SalaryEnum;
import com.example.experiencems.Constants.Tenure;
import com.example.experiencems.request.LoanRegRequest;
import com.example.experiencems.request.LoanRequest;
import com.example.experiencems.response.LoanRegResponse;
import com.example.experiencems.response.LoanResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@FeignClient(name = "LoanApplication", url = "${config.rest.service.getLoanUrl}")
public interface LoanServiceImpl {

    @PostMapping("/loan")
  public LoanResponse add(@RequestBody  LoanRequest request);

    @GetMapping("/gettenure")
    public ArrayList<String> read(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid);

    @PostMapping("/register")
    public LoanRegResponse LoanRegistration (@RequestParam CountryEnum countryEnum, @RequestParam String name, @RequestParam int age, @RequestParam Long national_id, @RequestParam SalaryEnum salaryEnum, @RequestParam int loanamount,@RequestParam int tenure);



}
