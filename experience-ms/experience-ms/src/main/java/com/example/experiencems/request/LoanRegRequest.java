package com.example.experiencems.request;

import com.example.experiencems.Constants.SalaryEnum;
import com.example.experiencems.Constants.Tenure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRegRequest {

    private String name;
    private int age;
    private long national_id;
    private SalaryEnum salaryEnum;
    private Tenure tenure;
}
