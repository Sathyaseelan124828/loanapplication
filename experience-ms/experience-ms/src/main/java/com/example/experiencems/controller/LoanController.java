package com.example.experiencems.controller;

import com.example.experiencems.Constants.CountryEnum;
import com.example.experiencems.Constants.SalaryEnum;
import com.example.experiencems.Constants.Tenure;
import com.example.experiencems.exception.NameCheckException;
import com.example.experiencems.request.LoanRegRequest;
import com.example.experiencems.request.LoanRequest;
import com.example.experiencems.response.LoanRegResponse;
import com.example.experiencems.response.LoanResponse;
import com.example.experiencems.service.LoanService;
import com.example.experiencems.service.LoanServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;

@RequestMapping("/loan-process")
@RestController
@Slf4j
public class LoanController {

    @Autowired
    LoanService loanService;

    @GetMapping
    public ArrayList<String> read(@RequestParam CountryEnum countryEnum, String uuid)
    {
        return loanService.read(countryEnum,uuid);
    }

    @PostMapping("/loan")
    public ResponseEntity<LoanResponse> add(LoanRequest request) {
        return new ResponseEntity(loanService.add(request), HttpStatus.OK);
    }


    @PostMapping("/register")
    public ResponseEntity<LoanRegResponse>LoanRegistraion(@RequestParam CountryEnum countryEnum, @RequestParam String name, @RequestParam int age, @RequestParam Long national_id, @RequestParam SalaryEnum salaryEnum, @RequestParam int loanamount, @RequestParam int tenure) {
        log.info("The values are passing");

        try {

            return new ResponseEntity(loanService.LoanRegistration(countryEnum, name, age, national_id, salaryEnum, loanamount, tenure), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return  new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }





}
