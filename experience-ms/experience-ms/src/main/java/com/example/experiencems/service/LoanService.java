package com.example.experiencems.service;

import com.example.experiencems.Constants.CountryEnum;
import com.example.experiencems.Constants.SalaryEnum;
import com.example.experiencems.Constants.Tenure;
import com.example.experiencems.controller.LoanController;
import com.example.experiencems.exception.LoanAmountException;
import com.example.experiencems.request.LoanRegRequest;
import com.example.experiencems.request.LoanRequest;
import com.example.experiencems.response.LoanRegResponse;
import com.example.experiencems.response.LoanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Service
public class LoanService {
    @Autowired
    LoanServiceImpl loanServiceImpl;

    public LoanResponse add(LoanRequest loanRequest) {
        if (loanRequest.getLoanAmount() > 600 || loanRequest.getLoanAmount() < 2000)
        {
            return loanServiceImpl.add(loanRequest);
        }
        else
        {
            throw new LoanAmountException();
        }

    }
    public ArrayList<String> read(CountryEnum countryEnum, String uuid)
    {
        ArrayList<String> month = new ArrayList<>();
        month.add("Months-60");
        month.add("Months-48");
        month.add("Months-36");
        month.add("Months-24");
        month.add("Months-12");
        return month;
    }

public LoanRegResponse LoanRegistration (@RequestParam CountryEnum countryEnum, @RequestParam String name, @RequestParam int age, @RequestParam Long national_id, @RequestParam SalaryEnum salaryEnum, @RequestParam int loanamount,@RequestParam int tenure) {
     {
        return  loanServiceImpl.LoanRegistration(countryEnum, name, age, national_id, salaryEnum, loanamount, tenure);

          }
}
}
