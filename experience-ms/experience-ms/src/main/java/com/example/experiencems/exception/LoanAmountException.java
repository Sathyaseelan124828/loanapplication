package com.example.experiencems.exception;

public class LoanAmountException extends RuntimeException{
    public LoanAmountException()
    {
        super("Loan amount should be in between 600JOD-2000JOD");
    }
}
