package com.customer.loan.model.response;


import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.Constants.SalaryEnum;
import com.customer.loan.Constants.StatusEnum;
import com.customer.loan.Constants.Tenure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRegResponse {

  StatusEnum statusEnum;


}
