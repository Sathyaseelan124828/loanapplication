package com.customer.loan.model.request;

import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.Constants.SalaryEnum;
import com.customer.loan.Constants.Tenure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRegRequest {

   private  CountryEnum countryEnum;
    private String name;
    private int age;
    private long national_id;
    private SalaryEnum salaryEnum;
    private int loanamount;
    private int tenure;
}
