package com.customer.loan.model.request;


import com.customer.loan.Constants.Tenure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest
{
    private float loanAmount;
    private int tenure;
}
