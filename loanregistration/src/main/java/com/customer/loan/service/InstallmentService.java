package com.customer.loan.service;

import com.customer.loan.Constants.Tenure;
import org.springframework.stereotype.Service;

@Service

public class InstallmentService {
    int calculateInstallment(double loanAmount,double loanInterestRate, float month)
    {
        float time = month/12;
        loanInterestRate = loanInterestRate/(12 * 100);
        time=time*12;
        Double result = (loanAmount * loanInterestRate * Math.pow(1 + loanInterestRate,time))/(Math.pow(1+ loanInterestRate, time)-1);
        int installment = (int) Math.round(result);
        return installment;
         }
}
