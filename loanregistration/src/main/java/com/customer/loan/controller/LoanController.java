package com.customer.loan.controller;

import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.Constants.SalaryEnum;
import com.customer.loan.Constants.Tenure;
import com.customer.loan.exception.AgeCheckException;
import com.customer.loan.exception.NameCheckException;
import com.customer.loan.exception.NationalIdCheckException;
import com.customer.loan.model.entity.LoanEntity;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.response.LoanRegResponse;
import com.customer.loan.model.response.LoanResponse;
import com.customer.loan.service.LoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping
@Slf4j
public class LoanController {
    @Autowired
    LoanService loanService;


    @GetMapping
    public ArrayList<String> read(@RequestParam CountryEnum countryEnum, String uuid) {
        return loanService.read(countryEnum, uuid);
    }


    @PostMapping("/loan")
    public ResponseEntity<LoanResponse> add(@RequestBody LoanRequest loanRequest) {

        return new ResponseEntity(loanService.add(loanRequest), HttpStatus.OK);
    }


    @PostMapping("/register")
    public ResponseEntity<LoanEntity> LoanRegistraion(@RequestParam CountryEnum countryEnum, @RequestParam String name, @RequestParam int age, @RequestParam long national_id, @RequestParam SalaryEnum salaryEnum,@RequestParam int loanamount, @RequestParam int tenure) {
        try {
            log.info("The values are passing");
            return new ResponseEntity(loanService.LoanRegistration(countryEnum, name, age, national_id, salaryEnum,loanamount, tenure), HttpStatus.OK);
        }
        catch (NameCheckException e)
        {
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_GATEWAY);
        }
        catch (AgeCheckException e)
        {
            return  new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
        catch (NationalIdCheckException e)
        {
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
        }




}

